<?php

	namespace App\Controllers;
	use Psr\Http\Message\ServerRequestInterface as Request;
	use Psr\Http\Message\ResponseInterface as Response;
	use App\Classes\UserElements;

	class UserController extends UserElements
	{
		protected $conn;
		protected $view;

	 	public function __construct($container) {
	  		$this->conn =  $container->pdo;
	  		$this->view =  $container->view;
	  	}

    	function user_login (Request $request, Response $response) {
    		//print_r($_SESSION); die;
    		if(isset($_SESSION['id']) && isset($_SESSION['success_flag']))
    		{
    			$result =  $this->check_last_activity_everytime($request,$response);
    			//print_r($_SESSION); die;
				if(isset($result))
				{
					return $result;
					// $_SESSION['login_msg'] = $result;
					// return $response->withRedirect(BASE_URL.'login');
				}
				else
				{
					return $response->withRedirect(BASE_URL.'dashboard');
				}
			}
			else
			{

				//session_destroy();
				unset($_SESSION["login_msg"]);
				return $this->view->render($response, 'login.twig');
			}
    	}

    	function user_login_process (Request $request, Response $response) {

    		if(isset($_POST) && !empty($_POST))
    		{
    			$request_data = $_POST;
    			$is_valid = $this->check_for_user_validity($request_data);
    			unset($_POST);
    			// print_r($is_valid); die;
    			if($is_valid['code'] == SUCCESS)
	    		{
	    			$is_valid['menu_data'] = $this->render_menu_list($_SESSION["user_id"]);
	    			return $this->view->render($response, 'dashboard.twig',$is_valid);
	    		}
	    		else if(isset($is_valid['redirect']))
	    		{
	    			$_SESSION['msg'] 		= $is_valid['msg'];
	    			$_SESSION['redirect'] 	= $is_valid['redirect'];
	    			return $response->withRedirect(BASE_URL.'change_password');
	    		}
	    		else if(isset($is_valid['logout']))
	    		{
	    			/*$new_timestamp = $this->conn->query("Update users set user_ip = '',user_session_id = '', logged_in = 0, last_activity_timestamp = '".date('Y-m-d H:i:s')."' where user_id =".$_SESSION["user_id"])->fetch();

				   	unset($_SESSION["id"]);
				   	unset($_SESSION["user_id"]);
				   	unset($_SESSION["user_name"]);
				   	$_SESSION['login_msg'] = $is_valid['msg'];
				   	session_destroy();*/
				   	$this->user_logout($request, $response, $is_valid['msg'], true);
				   	return $response->withRedirect(BASE_URL.'login');
	    		}
	    		else
	    		{
	    			$_SESSION['login_msg'] = $is_valid['msg'];
	    			return $response->withRedirect(BASE_URL.'login');
	    		}

	    	}
    		else
    		{
    			if(isset($_SESSION['id']) && isset($_SESSION['success_flag']))
    			{
    				$result =  $this->check_last_activity_everytime($request,$response);
    				//print_r($result);
    				if(isset($result) && !empty($result))
    				{
    					return $result;
    					// $_SESSION['login_msg'] = $result;
    					// return $response->withRedirect(BASE_URL.'login');
    				}
    				else
    				{
    					$is_valid['menu_data'] = $this->render_menu_list($_SESSION["user_id"]);
		    			$this->view->render($response, 'dashboard.twig', $is_valid);
    				}

				}
    			else
    			{	$_SESSION['login_msg'] = $is_valid['msg'];
					return $response->withRedirect(BASE_URL.'login');
    			}
    		}
		}

		private function check_for_user_validity($request_data)
		{
			// print_r($request_data);
			$UserElement = new UserElements($this->conn);
			$is_active = $UserElement::is_user_valid($request_data);

			return $is_active;
		}

		function user_logout (Request $request, Response $response,$login_msg, $is_forcefully_logout = false) {
			if(isset($_SESSION['id']) && isset($_SESSION['success_flag']))
			{
				$new_timestamp = $this->conn->query("Update users set user_ip = '',user_session_id = '', logged_in = 0, last_activity_timestamp = '".CURRENT_DATE."' where user_id =".$_SESSION["user_id"])->fetch();

				$general_pfail = array(
						'log_general_description' => 'user logout',
						'log_general_event_type'  => isset($is_forcefully_logout) && $is_forcefully_logout == true ? USER_FORCEFULLY_LOGGED_OUT : USER_LOGOUT,
						'created_timestamp'		  => CURRENT_DATE,
						'child_log'				  => json_encode(array('table'=>'log_logout'))
				);

				$UserElement = new UserElements($this->conn);
				$log_id = $UserElement::log_general_attempts($general_pfail);

				$log_logout = array(
					'log_general_id' => $log_id,
					'attempted_ip' => $UserElement::get_client_ip(),
					'user_id' => $_SESSION["user_id"],
					'logout_timestamp' => CURRENT_DATE
				);

				$UserElement::log_logout_log($log_logout);

			   	unset($_SESSION["id"]);
			   	unset($_SESSION["user_id"]);
		   		unset($_SESSION["user_name"]);
		   		unset($_SESSION["pass_msg"]);
			} elseif(isset($_SESSION['id']) && $is_forcefully_logout == true) {
				$new_timestamp = $this->conn->query("Update users set user_ip = '',user_session_id = '', logged_in = 0, last_activity_timestamp = '".CURRENT_DATE."' where user_id =".$_SESSION["user_id"])->fetch();

				$general_pfail = array(
					'log_general_description' => 'user logout',
					'log_general_event_type'  => USER_FORCEFULLY_LOGGED_OUT,
					'created_timestamp'		  => CURRENT_DATE,
					'child_log'				  => json_encode(array('table'=>'log_logout'))
				);

				$UserElement = new UserElements($this->conn);
				$log_id = $UserElement::log_general_attempts($general_pfail);

				$log_logout = array(
					'log_general_id' => $log_id,
					'attempted_ip' => $UserElement::get_client_ip(),
					'user_id' => $_SESSION["user_id"],
					'logout_timestamp' => CURRENT_DATE
				);

				$UserElement::log_logout_log($log_logout);

			   	unset($_SESSION["id"]);
			   	unset($_SESSION["user_id"]);
		   		unset($_SESSION["user_name"]);
		   		unset($_SESSION["pass_msg"]);
			}
			session_destroy();


			if(isset($login_msg) && !empty($login_msg))
			{
				return TRUE;
			}
			else
			{
				return $response->withRedirect(BASE_URL.'login');
			}

		}

    	private function check_user_ip_count()
		{
			$UserElement = new UserElements($this->conn);
			$client_ip 	 = $UserElement::get_client_ip();

			$get_count = $UserElement::get_max_attempt_count_by_ip($client_ip);
			return $get_count;
		}

    	function user_forgot_password(Request $request, Response $response,$is_exist)
    	{
    		if(isset($_POST) && !empty($_POST))
    		{
    			$name = pg_escape_string($_POST["username"]);
    			$is_exist = $this->check_for_user_exist($name);
    			//print_r($is_exist); die;
    			if($is_exist['code'] == SUCCESS)
	    		{
	    			//$_SESSION['msg'] 		= $is_exist['msg'];
    				$_SESSION['user_id'] 	= $is_exist['user_id'];
    				$_SESSION['user_name'] 	= $name;
    				$_SESSION['otp_flag'] 	= 1;
    				return $response->withRedirect(BASE_URL.'verify_otp');
	    		}
	    		else
	    			return $this->view->render($response, 'forgot_password.twig',$is_exist);
    		}
    		else
    		{
    			session_destroy();
    			return $this->view->render($response, 'forgot_password.twig');
    		}
    	}

    	function verify_otp_process(Request $request, Response $response)
    	{
    		if(isset($_POST) && !empty($_POST))
    		{
    			$data = array('otp' => $_POST['otp'], 'user_id' => $_POST['user_id'], 'user_name' => $_POST['user_name']);
    			if (is_numeric($data['otp']))
			  	{
			    	$otp = $data['otp'];
			    	$is_otp = $this->verify_for_user_otp($data);
			    	if($is_otp['code'] == SUCCESS)
	    			{
	    				$_SESSION['code'] 		= SUCCESS;
		    			return $response->withRedirect(BASE_URL.'new_password');
	    			}
		    		else if(isset($is_otp['user_blocked']))
		    		{
		    			$_SESSION['msg'] 		= $is_otp['msg'];
		    			return $response->withRedirect(BASE_URL.'forgot_password');
		    		}
		    		else
		    			return $this->view->render($response, 'otp.twig',$is_otp);
			  	}
			  	else
			  	{
					$UserElement = new UserElements($this->conn);
			  		$attempt_count = $UserElement::get_otp_attempt_count($data['user_id']);
					$attempt_count = $attempt_count + 1;
					if($attempt_count > LOG_COUNT)
					{
						$UserElement::inactive_user(INACTIVE,$data['user_id']);
						$_SESSION['msg'] 		= M1021;
		    			return $response->withRedirect(BASE_URL.'forgot_password');
			    	}
					else
					{
						$log_general = array(
							'log_general_description' => INVALID_OTP_DESC,
							'log_general_event_type'  => INVALID_OTP,
							'created_timestamp'		  => CURRENT_DATE,
							'child_log'				  => json_encode(array('table'=>OTP_ATTEMPT))
						);

						$log_id = $UserElement::log_general_attempts($log_general);

						$otp_attempts = array(
								'otp_id'  			=> 0,
								'log_general_id'  	=> $log_id,
								'attempted_otp'		=> pg_escape_string($data['otp']),
								'attempt_number'	=> ATTEMPT_FAIL,
								'attempted_user_id'	=> $data['user_id'],
								'attempted_timestamp'	=> CURRENT_DATE,
								'attempted_ip'		=> $UserElement::get_client_ip(),
						);
						$UserElement::log_otp_attempt($otp_attempts);

						$errData['otp_err'] = M1022;
				  		$errData['user_id'] = $data['user_id'];
				  		return $this->view->render($response, 'otp.twig',$errData);
			  		}

			  	}
    		}
    		else if(isset($_SESSION['otp_flag']) && !empty($_SESSION['otp_flag']))
    		{
    			unset($_SESSION['otp_flag']);
    			return $this->view->render($response, 'otp.twig',$_SESSION);
    		}
    		else
    			return $response->withRedirect(BASE_URL.'forgot_password');
		}

    	private function check_for_user_exist($request_data)
		{
			$existElement = new UserElements($this->conn);
			$is_exist = $existElement::is_user_exist($request_data);
			return $is_exist;
		}

		private function verify_for_user_otp($request_data)
		{
			//print_r($request_data); die;
			$verifyElement = new UserElements($this->conn);
			$is_exist = $verifyElement::is_otp_exist($request_data);
			return $is_exist;
		}

		function create_new_password($request, $response)
		{
			if(isset($_POST) && !empty($_POST))
			{
				$request_data = $_POST;
				$password_obj = new PasswordController($this->conn);
				if($request_data['n_pswd'] == $request_data['c_pswd'])
				{
					$result = $password_obj->check_password_complexity($request_data);
					// echo "<pre>";
					// print_r(json_encode($result));
					// die;
					if($result && !empty($result))
					{
						$errData['error_array'] = json_encode($result);

				  		$errData['user_id'] = $request_data['user_id'];
				  		return $this->view->render($response, 'new_password.twig', $errData );
					}
					else
					{
						$password_obj->update_user_password($request_data);
						$_SESSION['pass_msg'] = M1023;
						return $response->withRedirect(BASE_URL.'login');
					}
				}
				else
				{
					$errData['new_err'] = M1024;
			  		$errData['user_id'] = $request_data['user_id'];
			  		return $this->view->render($response, 'new_password.twig',$errData);
				}
			}
			if(isset($_SESSION['code']) && !empty($_SESSION['code']))
    		{
    			unset($_SESSION['code']);
    			return $this->view->render($response, 'new_password.twig',$_SESSION);
    		}
			else
				return $response->withRedirect(BASE_URL.'forgot_password');

		}

		function user_change_password($request, $response)
		{
			//check post data and pass post array to password complixity function.
			if(isset($_POST) && !empty($_POST))
			{
				$request_data = $_POST;
				$password_obj = new PasswordController($this->conn);

    			$old_pass = $password_obj->get_user_password($request_data['user_id']);
				//print_r($old_pass); die;
				if (password_verify($request_data['o_pswd'], $old_pass))
				{
					if($request_data['n_pswd'] == $request_data['c_pswd'])
					{
						$result = $password_obj->check_password_complexity($request_data);
						if($result && !empty($result))
						{
							// return $response;

							// echo "<pre>";
							// print_r(json_encode($result));
							// die;
							$errData['error_array'] = json_encode($result);

					  		$errData['user_id'] = $request_data['user_id'];
					  		return $this->view->render($response, 'change_password.twig', $errData );
						}
						else
						{
							$password_obj->update_user_password($request_data);
							$_SESSION['pass_msg'] = M1025;
							return $response->withRedirect(BASE_URL.'login');
						}

					}
					else
					{
						$errData['new_err'] = M1026;
				  		$errData['user_id'] = $request_data['user_id'];
				  		return $this->view->render($response, 'change_password.twig',$errData);
					}

				}
				else
				{
					$errData['old_err'] = M1027;
			  		$errData['user_id'] = $request_data['user_id'];
			  		return $this->view->render($response, 'change_password.twig',$errData);
				}
			}
			else if(isset($_SESSION['id']) && isset($_SESSION['redirect']))
			{
				//$this->check_last_activity_everytime($request,$response);
				unset($_SESSION["redirect"]);
				$data['user_id'] = $_SESSION['user_id'];
				return $this->view->render($response, 'change_password.twig',$data);
			}
			else
			{
				return $response->withRedirect(BASE_URL.'login');
			}

		}

		function resend_otp(Request $request, Response $response)
		{
			$uname = $_POST['user_name'];
			$name = pg_escape_string($uname);
			$is_exist = $this->check_for_user_exist($name);
			if($is_exist['code'] == SUCCESS)
    		{
    			$_SESSION['user_id'] 	= $is_exist['user_id'];
				$_SESSION['user_name'] 	= $name;
				$_SESSION['resend_flag'] 	= 1;
				$_SESSION['otp_flag'] 	= 1;
				$_SESSION['msg'] 		= M1028;
    			return $response->withRedirect(BASE_URL.'verify_otp');
    		}
    		else
    			return $this->view->render($response, 'forgot_password.twig',$is_exist);
    	}

		function check_last_activity_everytime($request,$response)
		{
			$timeElement 	= new UserElements($this->conn);

			$is_login 		= $timeElement::is_login($_SESSION['user_id'],$_SESSION["user_name"]);;
			if($is_login['code'] == FAIL)
			{
				$_SESSION['login_msg'] = $is_active['msg'];
				return $response->withRedirect(BASE_URL.'login');
				exit();
			}

			$is_active 		= $timeElement::is_user_active($_SESSION['user_id'],$_SESSION["user_name"]);

			if($is_active['code'] == FAIL)
			{
				//echo $is_active['msg']; die;
				$res= $this->user_logout($request,$response,$is_active['msg']);
				$_SESSION['login_msg'] = $is_active['msg'];
				return $response->withRedirect(BASE_URL.'login');
				exit();
			}

			$ip_change 		= $timeElement::is_multi_device($_SESSION['user_id'],$_SESSION["user_name"]);
			if($ip_change['code'] == FAIL)
			{
				if(isset($ip_change['logout']))
				{
					$res= $this->user_logout($request,$response,$ip_change['msg']);
					$_SESSION['login_msg'] = $ip_change['msg'];
					return $response->withRedirect(BASE_URL.'login');
					exit();
				}
				else
				{
					$_SESSION['login_msg'] = $ip_change['msg'];
					return $response->withRedirect(BASE_URL.'login');
					exit();
				}
			}
			$last_active  	= $timeElement::check_last_activity_everytime();
			if($last_active['code'] == FAIL)
			{
				$_SESSION['login_msg'] = $last_active['msg'];
				return $response->withRedirect(BASE_URL.'login');
				exit();
			}
			$pass_age		= $timeElement::check_password_age($_SESSION['user_id'],$_SESSION["user_name"]);
			if($pass_age['code'] == FAIL)
			{
				$_SESSION['redirect'] 	= $pass_age['redirect'];
				$_SESSION['msg'] = $pass_age['msg'];
				return $response->withRedirect(BASE_URL.'change_password');
				exit();
			}

		}

		private function render_menu_list($user_id)
		{
			$UserElement = new UserElements($this->conn);
			$menu_data = $UserElement::get_menu_list($user_id);

			if($menu_data['success'] == TRUE)
			{
				return $menu_data['data'];
			} else {
				return array();
			}
		}

	}

