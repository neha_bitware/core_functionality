<?php

namespace App\Controllers\admin;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Classes\UserElements;

class LoginController extends UserElements
{
	protected $conn;
	protected $view;

 	public function __construct($container) {
  		$this->conn =  $container->pdo;
  		$this->view =  $container->view;
  	}

  	public function login (Request $request, Response $response)
  	{
  		if(isset($_SESSION['admin']))
		{
			return $response->withRedirect(BASE_URL.'admin/dashboard');
		}
		else
		{
			//session_destroy();
			unset($_SESSION["login_msg"]);
			return $this->view->render($response, 'admin/login.twig');
		}
  	}

  	public function login_process (Request $request, Response $response)
  	{
  		if(isset($_POST) && !empty($_POST))
  		{
  			$request_data = $_POST;
  			$username = pg_escape_string($request_data['username']);
  			$password = $request_data['password'];

  			$user_data = $this->conn->query("SELECT * FROM admin WHERE username='".$username."' LIMIT 1")->fetch();

			if($user_data)
		    {
		    	if (password_verify($password, $user_data['password']))
			    {
			    	unset($_SESSION["login_msg"]);
			    	$_SESSION['admin'] = $user_data['admin_id'];
			 		return $this->view->render($response, 'admin/dashboard.twig',array());
			    } else {
			    	$_SESSION['login_msg'] = 'Invalid password.';
			    	return $response->withRedirect(BASE_URL.'admin');
			    }
		    } else {
		    	$_SESSION['login_msg'] = 'Invalid user.';
			    return $response->withRedirect(BASE_URL.'admin');
		    }
  		} else {
  			if(isset($_SESSION['admin']))
			{
				return $this->view->render($response, 'admin/dashboard.twig',array());
			} else {
				return $response->withRedirect(BASE_URL.'admin');
			}

  		}
  	}

  	public function admin_logout(Request $request, Response $response) {
  		unset($_SESSION["admin"]);
		session_destroy();
		return $response->withRedirect(BASE_URL.'admin');
  	}
 }