<?php

namespace App\Controllers\admin;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Classes\UserElements;

class UserController extends UserElements
{
	protected $conn;
	protected $view;

 	public function __construct($container) {
  		$this->conn =  $container->pdo;
  		$this->view =  $container->view;
  	}

  	public function index(Request $request, Response $response, $dataset = array()) {
  		$dataset['users'] = $this->conn->query("SELECT u.user_id, u.username, u.first_name, u.last_name, u.active, d.department_name, r.description, d.department_name FROM users as u INNER JOIN role as r ON u.role_id = r.role_id INNER JOIN department as d ON u.department_id = d.department_id order by u.user_id")->fetchAll();
  		// echo "<pre>";
  		// print_r($dataset);
  		// die;
  		return $this->view->render($response, 'admin/user_list.twig', $dataset);
  	}

  	public function add_user(Request $request, Response $response) {
  		//get all roles
  		$dataset = array();
  		unset($dataset['error']);
  		unset($dataset['success']);

  		$dataset['roles'] = $this->conn->query("SELECT * FROM role")->fetchAll();
  		$dataset['branches'] = $this->conn->query("SELECT * FROM branch")->fetchAll();
  		$dataset['departments'] = $this->conn->query("SELECT * FROM department")->fetchAll();

  		if(isset($_POST) && !empty($_POST))
    	{
			$post_data = $_POST;
			if(isset($post_data['role_id']) && $post_data['role_id'] == -1)
    		{
    			$dataset['error'] = "Please select role.";
				return $this->view->render($response, 'admin/add_user.twig', $dataset);
    		}
    		if(isset($post_data['department_id']) && $post_data['department_id'] == -1)
    		{
    			$dataset['error'] =  "Please select department.";
    			return $this->view->render($response, 'admin/add_user.twig', $dataset);
    		}
    		if(isset($post_data['branch_id']) && $post_data['branch_id'] == -1)
    		{
    			$dataset['error'] = "Please select branch.";
				return $this->view->render($response, 'admin/add_user.twig', $dataset);
    		}

    		if (preg_match('/([%\$#\*]+)/', $post_data['username']))
			{
			    $dataset['error'] = "User was not saved, Please enter a valid username.";
				return $this->view->render($response, 'admin/add_user.twig', $dataset);
			}

    		//Check if user with same username exist
			$check_username = $this->conn->query("SELECT * FROM users WHERE username='".$post_data['username']."' LIMIT 1")->fetch();
			if($check_username)
			{
				//User exist , please user another userame
				$dataset['error'] = "User exist, please user another userame.";
				return $this->view->render($response, 'admin/add_user.twig', $dataset);
			} else {
				//insert new user
				$password = password_hash('test123', PASSWORD_DEFAULT);
	    		$insert_sql = "INSERT INTO users (username,password,role_id,branch_id,department_id,first_name,last_name,last_activity_timestamp,active,logged_in,user_ip,user_session_id,user_phone_no) VALUES ('".$post_data['username']."','".$password."','".$post_data['role_id']."','".$post_data['branch_id']."','".$post_data['department_id']."','".$post_data['firstname']."','".$post_data['lastname']."','".date('Y-m-d H:i:s')."',1,'1','1','1',".$post_data['user_phone_no'].")";
	    		$insert_result = $this->conn->query($insert_sql);

	    		$dataset['success'] = "User added successfully.";
	    		return $this->index($request, $response, $dataset);
			}
    	} else {
	  		return $this->view->render($response, 'admin/add_user.twig', $dataset);
	  		// return $response->withRedirect(BASE_URL.'admin/add_user');
    	}
  	}
  	public function edit_user(Request $request, Response $response)
  	{
  		$user_id = $_GET['id'];
  		$user_data = $this->conn->query("SELECT * FROM users WHERE user_id = '".$user_id."'")->fetchAll();
  		return $this->view->render($response, 'admin/edit_user.twig', $user_data);
  	}
  	public function assign_template_to_user(Request $request, Response $response)
  	{
		if(isset($_POST) && !empty($_POST))
    	{
    		$post_data = $_POST;

    		$insert_sql = "INSERT INTO user_access (template_id, user_id) VALUES ('".$post_data['template_id']."','".$post_data['user_id']."')";
    		$insert_result = $this->conn->query($insert_sql);
    		if($insert_result)
    		{
    			$_SESSION['success'] = "Template added suessfully";
    		} else {
    			$_SESSION['error'] = $this->conn->result_error();
    		}
    		header('Location: '.BASE_URL.'admin/assign_template');
			exit;
		} else {

			if(isset($_SESSION['error']))
			{
				$dataset['error'] = $_SESSION['error'];
				unset($_SESSION['error']);
			} elseif(isset($_SESSION['success'])) {
				$dataset['success'] = $_SESSION['success'];
				unset($_SESSION['success']);
			}

			$user_access = $this->conn->query("SELECT u.username, at.template_name, ua.user_id FROM user_access as ua INNER JOIN users as u ON ua.user_id = u.user_id  INNER JOIN access_template as at ON ua.template_id = at.template_id")->fetchAll();
	  		if($user_access)
	  		{
	  			$dataset['user_access'] = $user_access;
	  			foreach ($user_access as $key => $value) {
	  				# code...
	  				$user_ids[] = $value['user_id'];
	  			}
	  		} else {
	  			$dataset['user_access'] = array();
	  		}

	  		$dataset['templates'] = $this->conn->query("SELECT * FROM access_template")->fetchAll();
	  		$dataset['users'] = $this->conn->query("SELECT * FROM users where NOT user_id = ANY('{".implode(',',$user_ids)."}')")->fetchAll();
	  		// print_r($dataset['users']);
	  		// die;
	  		return $this->view->render($response, 'admin/assign_template.twig', $dataset);
    	}
  	}


}