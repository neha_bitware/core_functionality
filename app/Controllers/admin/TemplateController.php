<?php

namespace App\Controllers\admin;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Classes\UserElements;

class TemplateController extends UserElements
{
	protected $conn;
	protected $view;

 	public function __construct($container) {
  		$this->conn =  $container->pdo;
  		$this->view =  $container->view;
  	}
  	public function index(Request $request, Response $response, $args = array())
  	{
  		$args['templates'] = $this->conn->query("SELECT a.template_id, a.template_name, r.description, d.department_name FROM access_template as a INNER JOIN role as r ON a.role_id = r.role_id INNER JOIN department as d ON a.department_id = d.department_id order by a.template_id")->fetchAll();
  		// echo "<pre>";
  		// print_r($dataset);
  		// die;
  		return $this->view->render($response, 'admin/templates.twig', $args);
    }

  	public function create(Request $request, Response $response)
  	{
  		$dataset = array();
  		unset($dataset['error']);
  		unset($dataset['success']);
  		if(isset($_POST) && !empty($_POST))
    	{
    		$post_data = $_POST;
    		// echo "<pre>";
    		// print_r($post_data);
    		// die;
    		if(isset($post_data['role_id']) && $post_data['role_id'] == -1)
    		{
    			$dataset['error'] = "Please select role.";
    			$dataset['roles'] = $this->conn->query("SELECT * FROM role")->fetchAll();
	  			$dataset['departments'] = $this->conn->query("SELECT * FROM department")->fetchAll();
	  			$dataset['menus'] = $this->render_all_menus();
				return $this->view->render($response, 'admin/create_template.twig', $dataset);

    		}
    		if(isset($post_data['department_id']) && $post_data['department_id'] == -1)
    		{
    			$dataset['error'] = "Please select department.";
				$dataset['roles'] = $this->conn->query("SELECT * FROM role")->fetchAll();
	  			$dataset['departments'] = $this->conn->query("SELECT * FROM department")->fetchAll();
	  			$dataset['menus'] = $this->render_all_menus();
				return $this->view->render($response, 'admin/create_template.twig', $dataset);
    		}


    		$insert_template_sql = "INSERT INTO access_template (template_name,role_id,department_id) VALUES ('".$post_data['template_name']."','".$post_data['role_id']."','".$post_data['department_id']."')";
	    	$insert_template_result = $this->conn->query($insert_template_sql);

	    	// echo "<pre>";
    		// print_r($lastInsertId);
    		// die;
    		if($insert_template_result)
    		{
    			$new_template_id = $this->conn->lastInsertId();
				$new_level = array();
    			$approval = $post_data['approval'];
    			$level = $post_data['level'];
    			foreach ($level as $key => $value)
				{
				    if ($value == '-1')
				    {
				        unset($level[$key]);
				    } else {
				    	$new_level[] = $value;
				    }
				}
    			if(isset($post_data['routes']) && !empty($post_data['routes']))
	    		{
	    			$routes = $post_data['routes'];
		    		for ($i=0; $i < count($routes) ; $i++) {
		    			# code...
		    			if(isset($routes[$i]) && $routes[$i])
		    			{
		    				if(isset($approval[$i]) && $approval[$i] && isset($new_level[$i]) && $new_level[$i])
				    		{
				    			$insert_sql = "INSERT INTO page_access (template_id, route_id, approval_required, level_for_approval) VALUES ('".$new_template_id."','".$routes[$i]."','yes',".$new_level[$i].")";
				    		} else {
								$insert_sql = "INSERT INTO page_access (template_id, route_id, approval_required) VALUES ('".$new_template_id."','".$routes[$i]."','no')";
				    		}

			    			$insert_result = $this->conn->query($insert_sql);
			    			if(!$insert_result)
				    		{
				    			$_SESSION['error'] = $this->conn->result_error();
				    		}
		    			}

		    		}
	    		}
	    		if(isset($post_data['submenu']) && !empty($post_data['submenu']))
	    		{
	    			foreach ($post_data['submenu'] as $key => $value) {
		    			# code...
		    			$insert_submenu_sql = "INSERT INTO page_access (template_id, route_id, approval_required, level_for_approval) VALUES ('".$new_template_id."','".$value."','".$approval_required."',".$post_data['level'].")";
		    			$insert_submenu_result = $this->conn->query($insert_submenu_sql);
		    			if(!$insert_submenu_result)
			    		{
			    			$_SESSION['error'] = $this->conn->result_error();
			    		}
		    		}
	    		}

	    		//Assign that template to user
	    		if(isset($post_data['user_list']) && !empty($post_data['user_list']))
	    		{
		    		foreach ($post_data['user_list'] as $key => $value) {
		    			# code...
		    			//if initially user exist, delete it
		    			$this->conn->query("DELETE FROM user_access where user_id = ".$value."");
		    			$insert_user_sql = "INSERT INTO user_access (template_id, user_id) VALUES ('".$new_template_id."','".$value."')";
		    			$insert_user_result = $this->conn->query($insert_user_sql);
		    			if(!$insert_user_result)
			    		{
			    			$_SESSION['error'] = $this->conn->result_error();
			    		}
		    		}
	    		}
			} else {
    			$_SESSION['error'] = "Error in adding Template.";
    		}

	    	$this->index($request, $response, $_SESSION);
	    } else {
    		//Get all pages and its routes
    		$dataset['roles'] = $this->conn->query("SELECT * FROM role")->fetchAll();
	  		$dataset['departments'] = $this->conn->query("SELECT * FROM department")->fetchAll();
	  		$dataset['menus'] = $this->render_all_menus();
	  		//$dataset['users'] = array();
			// echo "<pre>";
	  		// print_r($this->get_user_access_templates());
	  		// die;

	  		return $this->view->render($response, 'admin/create_template.twig', $dataset);
    	}
  	}
  	public function edit(Request $request, Response $response, $args = array())
	{
		$template_id = $_GET['id'];
  		$users_assigned_data = $this->conn->query("Select * FROM user_access where template_id = ".$template_id."")->fetchAll();
  		$template_details = $this->conn->query("Select * FROM access_template where template_id = ".$template_id."")->fetch();
  		$role_id = $template_details['role_id'];
  		$department_id = $template_details['department_id'];
  		$dataset['template_details'] = $template_details;
  		$routes = $this->conn->query("Select * FROM page_access INNER JOIN menus ON page_access.route_id = menus.route_id INNER JOIN page_routes ON page_access.route_id = page_routes.route_id where page_access.template_id = ".$template_id."")->fetchAll();
  		$dataset['roles'] = $this->conn->query("SELECT * FROM role")->fetchAll();
  		$dataset['departments'] = $this->conn->query("SELECT * FROM department")->fetchAll();
  		$menus = $this->render_all_menus();
  		// $dataset['menus'] = $this->render_all_menus();

  		$data = array();
  		$menu_item_id_array = array();
  		$approval_menu_array = array();
  		foreach ($routes as $key => $value) {
  			# code...
  			$temp = array();
  			$temp['template_id'] = $value['template_id'];
  			$temp['route_id'] = $value['route_id'];
  			$temp['approval_required'] = $value['approval_required'];
  			$temp['level_for_approval'] = $value['level_for_approval'];
  			$temp['menu_item_id'] = $value['menu_item_id'];
  			$temp['menu_parent_id'] = $value['menu_parent_id'];
  			$temp['menu_name'] = $value['menu_name'];
  			$temp['route_name'] = $value['route_name'];
  			$temp['route_url'] = $value['route_url'];
  			$data[] = $temp;
  			$menu_item_id_array[] = $value['menu_item_id'];
  			if($value['level_for_approval'])
  			{
  				$approval_menu_array[] = $value['menu_item_id'];
  				$level[$value['menu_item_id']] = $value['level_for_approval'];
  			}
  		}
  		$dataset['approval_menu_array'] = $approval_menu_array;
  		$dataset['level'] = $level;
  		$menu_data = array();
  		foreach ($menus as $menu_key => $menu_value) {
  			if(!empty($menu_value['sub_menus']))
  			{
  				foreach ($menu_value['sub_menus'] as $key => $value) {
					if(in_array($value['menu_item_id'], $menu_item_id_array))
		  			{
		  				$menus[$menu_key]['sub_menus'][$key]['is_checked']=1;
		  			} else {
		  				$menus[$menu_key]['sub_menus'][$key]['is_checked']=0;
		  			}
		  			if(in_array($value['menu_item_id'], $approval_menu_array))
		  			{
		  				$menus[$menu_key]['sub_menus'][$key]['approval_required']=1;
		  			} else {
		  				$menus[$menu_key]['sub_menus'][$key]['approval_required']=0;
		  			}
		  		}
  			}
	  	}
	  	$dataset['menus'] = $menus;

	  	//Get all users with that role and department
	  	$role_dept_users = $this->conn->query("Select * FROM users where role_id = ".$role_id." AND department_id = ".$department_id."")->fetchAll();
	  	foreach ($role_dept_users as $key => $value) {
  			# code...
  			$temp = array();
  			$temp['user_id'] = $value['user_id'];
  			$temp['username'] = $value['username'];
  			$users[] = $temp;
  		}
  		$dataset['users'] = $users;

  		foreach ($users_assigned_data as $key => $value) {
  			# code...
  			$users_assigned[] = $value['user_id'];
  		}
  		$dataset['users_assigned'] = $users_assigned;
  		$dataset['template_id'] = $template_id;
  		return $this->view->render($response, 'admin/edit_template.twig', $dataset);

  	}
  	public function update()
  	{
  		$post_data = $_POST;
  		echo "<pre>";
  		print_r($post_data);
  		die;
  		$template_id = $post_data['template_id'];
		$update_template_sql = "UPDATE access_template SET template_name = '".$post_data['template_name']."' , role_id = '".$post_data['role_id']."', department_id = '".$post_data['department_id']."' WHERE template_id = ".$template_id."";
    	$update_template_result = $this->conn->query($update_template_sql);

    	// echo "<pre>";
		// print_r($lastInsertId);
		// die;
		if($update_template_result)
		{
			$new_level = array();
			$approval = $post_data['approval'];
			$level = $post_data['level'];
			foreach ($level as $key => $value)
			{
			    if ($value == '-1')
			    {
			        unset($level[$key]);
			    } else {
			    	$new_level[] = $value;
			    }
			}
			if(isset($post_data['routes']) && !empty($post_data['routes']))
    		{
    			$routes = $post_data['routes'];
	    		for ($i=0; $i < count($routes) ; $i++) {
	    			# code...
	    			if(isset($routes[$i]) && $routes[$i])
	    			{
	    				if(isset($approval[$i]) && $approval[$i] && isset($new_level[$i]) && $new_level[$i])
			    		{
			    			$insert_sql = "INSERT INTO page_access (template_id, route_id, approval_required, level_for_approval) VALUES ('".$new_template_id."','".$routes[$i]."','yes',".$new_level[$i].")";
			    		} else {
							$insert_sql = "INSERT INTO page_access (template_id, route_id, approval_required) VALUES ('".$new_template_id."','".$routes[$i]."','no')";
			    		}

		    			$insert_result = $this->conn->query($insert_sql);
		    			if(!$insert_result)
			    		{
			    			$_SESSION['error'] = $this->conn->result_error();
			    		}
	    			}

	    		}
    		}
    		if(isset($post_data['submenu']) && !empty($post_data['submenu']))
    		{
    			foreach ($post_data['submenu'] as $key => $value) {
	    			# code...
	    			$insert_submenu_sql = "INSERT INTO page_access (template_id, route_id, approval_required, level_for_approval) VALUES ('".$new_template_id."','".$value."','".$approval_required."',".$post_data['level'].")";
	    			$insert_submenu_result = $this->conn->query($insert_submenu_sql);
	    			if(!$insert_submenu_result)
		    		{
		    			$_SESSION['error'] = $this->conn->result_error();
		    		}
	    		}
    		}

    		//Assign that template to user
    		if(isset($post_data['user_list']) && !empty($post_data['user_list']))
    		{
	    		foreach ($post_data['user_list'] as $key => $value) {
	    			# code...
	    			//if initially user exist, delete it
	    			$this->conn->query("DELETE FROM user_access where user_id = ".$value."");
	    			$insert_user_sql = "INSERT INTO user_access (template_id, user_id) VALUES ('".$new_template_id."','".$value."')";
	    			$insert_user_result = $this->conn->query($insert_user_sql);
	    			if(!$insert_user_result)
		    		{
		    			$_SESSION['error'] = $this->conn->result_error();
		    		}
	    		}
    		}
		} else {
			$_SESSION['error'] = "Error in adding Template.";
		}

    	$this->index($request, $response, $_SESSION);
  	}
  	public function get_role_and_dept_based_users()
  	{
  		$post_data = $_POST;
  		$users = $this->get_user_access_templates($post_data['role_id'], $post_data['department_id']);
  		// echo $_POST;
  		echo json_encode($users);
  	}
  	public function ajax_check_user_template()
  	{
  		$post_data = $_POST;
  		$data = $this->conn->query("Select user_id from user_access WHERE user_id = ".$post_data['user_id']."")->fetchAll();
  		if(!empty($data))
  		{
  			echo "1";
  		} else {
  			echo "0";
  		}
  	}
  	private function get_user_access_templates($role_id, $department_id)
  	{
  		$data = array();
		$user_template_data = $this->conn->query("Select u.user_id, at.template_id, u.role_id, u.department_id, u.username from user_access as ua left join access_template as at on ua.template_id = at.template_id right join users as u on u.user_id = ua.user_id WHERE u.role_id = ".$role_id." AND u.department_id = ".$department_id."")->fetchAll();
		foreach ($user_template_data as $key => $value) {
			# code...
			$temp = array();
			$temp['template_id'] = $value['template_id'];
			$temp['user_role_id'] = $value['role_id'];
			$temp['user_department_id'] = $value['department_id'];
			$temp['username'] = $value['username'];
			$temp['user_id'] = $value['user_id'];
			$data[] = $temp;
		}
		return $data;
  	}
  	private function render_all_menus()
	{
		$data = $this->conn->query("Select * from menus left join page_routes on menus.route_id = page_routes.route_id")->fetchAll();
		$menus = array();
		$sub_menus = array();
		foreach ($data as $key => $value) {
			# code...
			if($value['menu_parent_id'] == 0)
			{
				$temp_menus = array();
				$temp_menus['menu_item_id'] = $value['menu_item_id'];
				$temp_menus['menu_name'] = $value['menu_name'];
				$menus[] = $temp_menus;
			} else {
				$temp = array();
				$temp['menu_item_id'] = $value['menu_item_id'];
				$temp['route_id'] = $value['route_id'];
				$temp['menu_parent_id'] = $value['menu_parent_id'];
				$temp['menu_name'] = $value['menu_name'];
				$sub_menus[$value['menu_parent_id']][] = $temp;
			}
		}
		foreach ($menus as $mkey => $mvalue) {
			# code...
			//if($mvalue['menu_item_id'])
			$temp = array();
			$temp['menu_item_id'] = $mvalue['menu_item_id'];
			$temp['menu_name'] = $mvalue['menu_name'];
			if(isset($sub_menus[$mvalue['menu_item_id']]))
			{
				$temp['sub_menus'] = $sub_menus[$mvalue['menu_item_id']];
			}
            $menu_data[] = $temp;
		}

		return $menu_data;
	}
  	public function assign(Request $request, Response $response, $args = array())
  	{
  		if(isset($_POST) && !empty($_POST))
    	{
    		$post_data = $_POST;
    		if($post_data['approval'])
    		{
    			$approval_required = "yes";
    		} else {
    			$approval_required = "no";
    		}
    		$insert_sql = "INSERT INTO page_access (template_id, route_id, approval_required, level_for_approval) VALUES ('".$post_data['template_id']."','".$post_data['route_id']."','".$approval_required."',".$post_data['level'].")";
    		$insert_result = $this->conn->query($insert_sql);
    		if($insert_result)
    		{
    			$_SESSION['success'] = "Route added suessfully";
    		} else {
    			$_SESSION['error'] = $this->conn->result_error();
    		}
    		$_SESSION['template_id'] = $post_data['template_id'];
    		header('Location: '.BASE_URL.'admin/assign_routes');
			exit;
		} else {
			if(isset($_GET) && !empty($_GET))
			{
				unset($_SESSION['success']);
		  		unset($_SESSION['error']);
		  		unset($_SESSION['template_id']);
				$template_id = $_GET['id'];
			} else {
				if(isset($_SESSION['error']))
				{
					$dataset['error'] = $_SESSION['error'];
					unset($_SESSION['error']);
				} elseif(isset($_SESSION['success'])) {
					$dataset['success'] = $_SESSION['success'];
					unset($_SESSION['success']);
				}
				$template_id = $_SESSION['template_id'];
				unset($_SESSION['template_id']);
			}

	  		$template_details = $this->conn->query("SELECT * FROM access_template WHERE template_id = ".$template_id)->fetch();
	  		if($template_details)
	  		{
	  			$dataset['template_name'] = $template_details['template_name'];
	  			$dataset['template_id'] = $template_details['template_id'];
	  		}
	  		$dataset['routes'] = $this->conn->query("SELECT * FROM page_routes")->fetchAll();
	  		$dataset['page_access'] = $this->conn->query("SELECT pa.level_for_approval, pa.approval_required, pg.route_name FROM page_access as pa INNER JOIN page_routes as pg ON pa.route_id = pg.route_id where pa.template_id = ".$template_id)->fetchAll();
	  		return $this->view->render($response, 'admin/assign_page_access.twig', $dataset);
    	}
  	}
}