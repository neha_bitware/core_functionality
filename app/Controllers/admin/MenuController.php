<?php

namespace App\Controllers\admin;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Classes\UserElements;

class MenuController extends UserElements
{
	protected $conn;
	protected $view;

 	public function __construct($container) {
  		$this->conn =  $container->pdo;
  		$this->view =  $container->view;
  	}
  	public function index(Request $request, Response $response, $args = array())
  	{
  		$args['menus'] = $this->conn->query("SELECT * FROM menus")->fetchAll();
  		// echo "<pre>";
  		// print_r($dataset);
  		// die;
  		return $this->view->render($response, 'admin/menus.twig', $args);
    }
    public function edit_menu_structure(Request $request, Response $response, $args = array())
    {
      	$menu_data = $this->render_all_menus();
      	$datalist['unassigned_menus'] = $menu_data['unassigned'];
      	unset($menu_data['unassigned']);
      	$datalist['menus'] = $menu_data;
      // echo "<pre>";
      //   print_r($this->render_all_menus());
      //   die;
      return $this->view->render($response, 'admin/menu_structure.twig', $datalist);
    }
    public function update_menu_structure()
    {
      $post_data = $_POST;
      echo "<pre>";
      print_r($post_data);
      die;
      $sub_menus = $post_data['sub_menus'];
      foreach ($sub_menus as $menu_id => $value) {
        # code...
        foreach ($value as $key => $sub_menu_id) {
        	# code...
        	if($menu_id != $sub_menu_id)
        	{
        		$update_sql = "UPDATE menus SET menu_parent_id = ".$menu_id." WHERE menu_item_id = ".$sub_menu_id."";
        		$this->conn->query($update_sql);
        	}
        }
      }
    }
    private function render_all_menus()
    {
      $data = $this->conn->query("Select * from menus left join page_routes on menus.route_id = page_routes.route_id")->fetchAll();
      $menus = array();
      $sub_menus = array();
      foreach ($data as $key => $value) {
        # code...
        if($value['menu_parent_id'] == 0)
        {
          $temp_menus = array();
          $temp_menus['menu_item_id'] = $value['menu_item_id'];
          $temp_menus['menu_name'] = $value['menu_name'];
          $menus[] = $temp_menus;
        } else {
          $temp = array();
          $temp['menu_item_id'] = $value['menu_item_id'];
          $temp['route_id'] = $value['route_id'];
          $temp['menu_parent_id'] = $value['menu_parent_id'];
          $temp['menu_name'] = $value['menu_name'];
          $sub_menus[$value['menu_parent_id']][] = $temp;
        }
      }
      foreach ($menus as $mkey => $mvalue) {
        # code...
        //if($mvalue['menu_item_id'])

        if(isset($sub_menus[$mvalue['menu_item_id']]))
        {
        	$temp = array();
	        $temp['menu_item_id'] = $mvalue['menu_item_id'];
	        $temp['menu_name'] = $mvalue['menu_name'];
        	$temp['sub_menus'] = $sub_menus[$mvalue['menu_item_id']];
        	$menu_data[] = $temp;
    	} else {
    		$temp = array();
	        $temp['menu_item_id'] = $mvalue['menu_item_id'];
	        $temp['menu_name'] = $mvalue['menu_name'];
        	$menu_data['unassigned'][] = $temp;
    	}

      }

      return $menu_data;
    }
}