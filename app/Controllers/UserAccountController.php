<?php

	namespace App\Controllers;
	use Psr\Http\Message\ServerRequestInterface as Request;
	use Psr\Http\Message\ResponseInterface as Response;
	use App\Classes\UserElements;

	class UserAccountController extends UserElements
	{
		protected $conn;
		protected $view;
	 	public function __construct($container) {
	  		$this->conn =  $container->pdo;
	  		$this->view =  $container->view;
	  	}

	  	public function get_user_profile(Request $request, Response $response){

	  		$is_valid['menu_data'] = $this->render_menu_list($_SESSION["user_id"]);
	  		return $this->view->render($response, 'user_profile.twig', $is_valid);
	  		//return $response->withRedirect(BASE_URL.'user_profile');
		}

	  	public function get_user_account_detail(Request $request, Response $response){

	  		$is_valid['menu_data'] = $this->render_menu_list($_SESSION["user_id"]);
			return $this->view->render($response, 'user_account_detail.twig', $is_valid);
	  		//return $response->withRedirect(BASE_URL.'user_account_detail');
		}

		private function render_menu_list($user_id)
		{
			$UserElement = new UserElements($this->conn);
			$menu_data = $UserElement::get_menu_list($user_id);

			if($menu_data['success'] == TRUE)
			{
				return $menu_data['data'];
			} else {
				return array();
			}
		}
	}