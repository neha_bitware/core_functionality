<?php

	namespace App\Controllers;
	use Psr\Http\Message\ServerRequestInterface as Request;
	use Psr\Http\Message\ResponseInterface as Response;
	use App\Classes\UserElements;

	class PasswordController extends UserElements
	{
		protected $conn;

	 	public function __construct($conn) {
	  		$this->conn =  $conn;

	  	}

	  	public function check_password_complexity($request_data){
	  		return $this->get_password_complexities($request_data);
		}

	  	private function get_password_complexities($request_data) {
	  		$password_complexity = $this->conn->query("Select * from system_config where param_name = '".PASSWORD_COMPLEXITY."'")->fetch();

	  		if(!empty($password_complexity))
	  		{
	  			$param_value = explode(',', $password_complexity['param_value']);
	  			$detail_description = $this->conn->query("Select * from password_complexity where complexity_id = ANY('{".$password_complexity['param_value']."}')")->fetchAll();
	  			$error = array();
	  			// $new_password = 'we';
	  			$new_password = $request_data['n_pswd'];
	  			foreach ($detail_description as $key => $value) {
	  				# code...
	  				if($value['complexity_id'] == COMPLEXITY_ID)
	  				{
	  					if(strlen($new_password) < $value['complexity_string'])

	  					{
	  						$error[] = $value['complexity_description'].' '.$value['complexity_string'];
	  					}
	  				} else {
	  					if(!preg_match($value['complexity_string'], $new_password))
		  				{
		  					$error[] = $value['complexity_description'];
		  				}
	  				}
	  			}
	  			return $error;

	  		} else {
	  			return false;
	  		}
	  	}

	  	public function get_user_password($user_id)
	  	{
	  		$userPassword = $this->conn->query("Select password from users where user_id = ".$user_id)->fetch();
	  		// $userPassword = $this->conn->query("Select password from users where user_id = 7")->fetch();
	  		return $userPassword['password'];
	  	}

	  	public function update_user_password($data)
	  	{
	  		$new_password = password_hash($data['n_pswd'], PASSWORD_BCRYPT);
	  		$old_password = password_hash($data['o_pswd'], PASSWORD_BCRYPT);
	  		//update new password in user table
	  		$this->conn->query("Update users set password = '".$new_password."' , last_activity_timestamp= '".CURRENT_DATE."' where user_id =".$data['user_id'])->fetch();

	  		$child_log = json_encode(array('table'=>'password_log'));
	  		//insert general log for password change
	  		$log = 	$this->conn->query("INSERT INTO log_general (log_general_description, log_general_event_type, created_timestamp, child_log) VALUES ('".PASSWORD_CHANGED_DESC."', '".PASSWORD_CHANGED."', '".CURRENT_DATE."', '".$child_log."') RETURNING log_general_id")->fetch();

			$log_id = $log['log_general_id'];

	  		//insert new password in password log table for that user
	  		$this->conn->query("INSERT INTO password_log (user_id, old_password,new_password, last_updated,log_general_id) VALUES ('".$data['user_id']."', '".$old_password."', '".$new_password."','".CURRENT_DATE."','".$log_id."')")->fetch();
	  	}
	}