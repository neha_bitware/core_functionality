<?php

namespace App\Controllers;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Classes\UserElements;

class BillingAndPaymentsController extends UserElements
{
	protected $conn;
	protected $view;

 	public function __construct($container) {
  		$this->conn =  $container->pdo;
	  	$this->view =  $container->view;
  	}

  	public function get_transaction_history(Request $request, Response $response){
  		// return $response->withRedirect(BASE_URL.'transaction_history');
  		$is_valid['menu_data'] = $this->render_menu_list($_SESSION["user_id"]);
  		return $this->view->render($response, 'transaction_history.twig', $is_valid);
	}

  	public function transfer_payments(Request $request, Response $response){
  		$is_valid['menu_data'] = $this->render_menu_list($_SESSION["user_id"]);
  		return $this->view->render($response, 'transfer_payments.twig', $is_valid);
	}

	private function render_menu_list($user_id)
	{
		$UserElement = new UserElements($this->conn);
		$menu_data = $UserElement::get_menu_list($user_id);

		if($menu_data['success'] == TRUE)
		{
			return $menu_data['data'];
		} else {
			return array();
		}
	}
}