<?php
namespace App\Classes;

class UserElements
{
	protected $conn;
	protected function __construct($container) {
		$this->conn =  $container;
	}

	protected function is_user_valid($data)
	{
		//Chek if username is correct
		$response = array();
		$client_ip = $this->get_client_ip();

		$username = pg_escape_string($data['username']);
		// die;
		// $username = preg_replace("/<!--.*?-->/", "", str_replace(' ','',$data['username']));
		$user_result = $this->check_username($username);

		if(!empty($user_result['data'])) {
			$user_id = $user_result['data']['user_id'];
			// $user_name = $user_result['data']['username'];

			$is_login = $this->is_login($user_id, $username);
			//if($is_login['success']) {
				$is_user_active = $this->is_user_active($user_id, $username);

				if($is_user_active['success'])
		    	{
					if (password_verify($data['password'], $user_result['data']['password']))
			    	{
			    		$count = $this->get_max_attempt_count_log($user_id, $username);
			    		if($count >= LOG_COUNT)
			    		{
			    			$this->inactive_user(BLOCKED,$user_id);
			    			$response = ['success' => false, 'code' => FAIL, 'msg' => M1007];
			    			$multiple_device = $this->is_multi_device($user_id, $username);
			    			$last_activity = $this->check_last_activity_timeout($user_id);
			    			$password_age = $this->check_password_age($user_id,$username);
						} else {
							$_SESSION["id"] 	   = session_id();
							$_SESSION["user_id"]   = $user_id;
							$_SESSION["user_name"] = $username;
							// Check whether user is on multiple device.
							$multiple_device = $this->is_multi_device($user_id, $username);
							if($multiple_device['success'])
					    	{
					    		$last_activity = $this->check_last_activity_timeout($user_id);
								if($last_activity['success'])

					    		{
					    			$password_age = $this->check_password_age($user_id,$username);
					    			if($password_age['success'])
						    		{
						    			$_SESSION["success_flag"] = 1;
			    						//Update session id in user table
										$this->conn->query("Update users set user_session_id = '".$_SESSION["id"]."', logged_in = 1, user_ip = '".$client_ip."' where user_id =".$user_id)->fetch();

										//log the user on successful login
										$log_general = array(
											'log_general_description' => 'user logged in',
											'log_general_event_type'  => LOGIN_SUCCESS,
											'created_timestamp'		  => CURRENT_DATE,
											'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
										);

										$log_attempt = array(
											'attempted_username'  => $username,
											'attempted_password'  => '',
											'attempted_ip'		  => $client_ip,
											'attempt_number'      => ATTEMPT_SUCCESS,
											'attempted_user_id'	  => $user_id,
											'attempted_timestamp' => CURRENT_DATE
										);
										$log_id 		= $this->log_general_attempts($log_general);
										$log_login  	= $this->log_login_attempts($log_attempt,$log_id);

										$response = $password_age;
						    		} else {
						    			$response = $password_age;
						    		}
					    		} else {
					    			$response =  $last_activity;
					    			$password_age = $this->check_password_age($user_id,$username);
					    		}
					    	} else {
						    	$response =  $multiple_device;
						    	$last_activity = $this->check_last_activity_timeout($user_id);
			    				$password_age = $this->check_password_age($user_id,$username);
							}
						}
			    	} else {
			    		//log user in table with attempt and password tried
						//Get initail count
			    		$count = $this->get_max_attempt_count_log($user_id, $username);
			    		//add password attempt
			    		$count = $count + 1;
			    		if($count > LOG_COUNT)
			    		{
			    			$this->inactive_user(BLOCKED,$user_id);
			    			$response =  ['success' => false, 'code' => FAIL, 'msg' => M1007];
						} else {
			    			//log the user when username is correct but password is wrong
							$general_pfail = array(
									'log_general_description' => 'user trying login',
									'log_general_event_type'  => LOGIN_FAIL_PASSWORD_WRONG,
									'created_timestamp'		  => CURRENT_DATE,
									'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
							);

							$attempt_pfail = array(
									'attempted_username'  => $username,
									'attempted_password'  => $data['password'],
									'attempted_ip'		  => $client_ip,
									'attempt_number'      => ATTEMPT_FAIL,
									'attempted_user_id'	  => $user_id,
									'attempted_timestamp' => CURRENT_DATE
							);

							$log_id 		= $this->log_general_attempts($general_pfail);
							$this->log_login_attempts($attempt_pfail,$log_id);

			    			$response =  ['success' => false, 'code' => FAIL, 'msg' => M1018];
			    		}

			    	}
			    } else {
					$response =  $is_user_active;
					$multiple_device = $this->is_multi_device($user_id, $username);
	    			$last_activity = $this->check_last_activity_timeout($user_id);
	    			$password_age = $this->check_password_age($user_id,$username);

	    			//log the user on successful login
					$log_general = array(
						'log_general_description' => 'user login failed',
						'log_general_event_type'  => LOGIN_FAIL,
						'created_timestamp'		  => CURRENT_DATE,
						'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
					);

					$log_attempt = array(
						'attempted_username'  => $username,
						'attempted_password'  => '',
						'attempted_ip'		  => $client_ip,
						'attempt_number'      => ATTEMPT_SUCCESS,
						'attempted_user_id'	  => $user_id,
						'attempted_timestamp' => CURRENT_DATE
					);
					$log_id 		= $this->log_general_attempts($log_general);
					$log_login  	= $this->log_login_attempts($log_attempt,$log_id);
			    }
			// } else {
			// 	//user is logged in
			// 	$response =  $is_login;
			// }
		} else {
			//Get initail count
    		$count = $this->get_max_attempt_count_by_ip($client_ip, $username);
    		//add password attempt

    		$count = $count + 1;
    		if($count > LOG_COUNT)
    		{
    			$response =  ['success' => false, 'code' => FAIL, 'msg' => M1019];
    		} else {
    			//log the user on successful login
				$response =  ['success' => false, 'code' => FAIL, 'msg' => M1020];
			}
		}
		return $response;
	}

	protected function check_username($username) {
		$success = $this->conn->query("SELECT * FROM users WHERE username='".$username."' LIMIT 1")->fetch();
		if($success)
	    {
	    	$user_id  = $success['user_id'];
	    	$general_pfail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => VALID_USERNAME,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_pfail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);


			$log_id = $this->log_general_attempts($general_pfail);
			$this->log_login_attempts($attempt_pfail,$log_id);

			$result = ['msg' => VALID_USERNAME, "status" =>true, "data" => $success];

	    } else {
	    	$general_pfail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => INVALID_USERNAME,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_pfail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_FAIL,
				'attempted_user_id'	  => 0,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_pfail);
			$this->log_login_attempts($attempt_pfail,$log_id);
			$result = ['msg' => INVALID_USERNAME, "status" =>false, "data" => array()];

	    }
	    return $result;
	}
	protected function is_login($user_id, $username) {
		$success = $this->conn->query("SELECT * FROM users WHERE user_id='".$user_id."' LIMIT 1")->fetch();
		if($success['logged_in'] == ACTIVE && $success['user_session_id'] == $_SESSION['id'])
	    {
	    	$general_pfail = array(
				'log_general_description' => 'user already logged in',
				'log_general_event_type'  => ALREADY_LOGIN,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_pfail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_pfail);
			$this->log_login_attempts($attempt_pfail,$log_id);

			//return ['success' => false, 'code' => FAIL, 'msg' => 'You are already logged into one device, Please logout first and then try to login'];

	    }
	    else {
	    	$general_pfail = array(
				'log_general_description' => 'user is newly logging in',
				'log_general_event_type'  => USER_LOGIN_ATTEMPT,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_pfail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => 0,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_pfail);
			$this->log_login_attempts($attempt_pfail,$log_id);
			return ['success' => false, 'code' => FAIL, 'msg' => M1001];

	    }
	}

	/*
	Function:
	Desc:
	Date: 11/10/2018, 12/10/2018
	Developer:
	*/
	/*protected function is_user_valid_old($data)
	{
		$error = array();
		$success = $this->conn->query("SELECT * FROM users WHERE username='".$data['username']."' LIMIT 1")->fetch();
		$client_ip  = $this->get_client_ip();
	    if($success)
	    {
	    	$password = $success['password'];
		    $user_id  = $success['user_id'];
	    	$is_user_active = $this->is_user_active($user_id);
	    	if($is_user_active['success'])
	    	{
				if (password_verify($data['password'], $success['password']))
		    	{
		    		$count = $this->get_max_attempt_count_log($user_id);
		    		//$ip_count   = $this->get_max_attempt_count_by_ip($client_ip);
		    		if($count >= LOG_COUNT)
		    		{
		    			$general_pfail = array(
								'log_general_description' => 'user trying login',
								'log_general_event_type'  => LOGIN_FAIL_MAX_PASSWORD_ATTEMPT,
								'created_timestamp'		  => CURRENT_DATE,
								'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
						);

						$attempt_pfail = array(
								'attempted_username'  => $data['username'],
								'attempted_password'  => '',
								'attempted_ip'		  => $client_ip,
								'attempt_number'      => ATTEMPT_SUCCESS,
								'attempted_user_id'	  => $user_id,
								'attempted_timestamp' => CURRENT_DATE
						);

						$log_id = $this->log_general_attempts($general_pfail);
		    			$this->inactive_user(BLOCKED,$user_id);
		    			return ['success' => false, 'code' => FAIL, 'msg' => 'You have reached the maximum level of password attempt. Please try login tomorrow'];
					}
					// else if($ip_count >= LOG_COUNT)
					// {
					// 	return ['success' => false, 'code' => FAIL, 'msg' => 'Your ip has been blocked.'];
					// }
					else
					{


						$_SESSION["id"] 	 = session_id();
						$_SESSION["user_id"] = $user_id;
						// Check whether user is on multiple device.
						$multiple_device = $this->is_multi_device($user_id);
				    	//print_r($multiple_device);
				    	if($multiple_device['success'])
				    	{
				    		// Check last activity timeout.
							$last_activity = $this->check_last_activity_timeout($user_id);
							if($last_activity['success'])
				    		{
				    			$password_age = $this->check_password_age($user_id);
				    			if($password_age['success'])
					    		{
					    			$_SESSION["success_flag"] = 1;
		    						//Update session id in user table
									$this->conn->query("Update users set user_session_id = '".$_SESSION["id"]."', logged_in = 1, user_ip = '".$client_ip."' where user_id =".$user_id)->fetch();

									//log the user on successful login
									$log_general = array(
										'log_general_description' => 'user logged in',
										'log_general_event_type'  => LOGIN_SUCCESS,
										'created_timestamp'		  => CURRENT_DATE,
										'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
									);

									$log_attempt = array(
										'attempted_username'  => $data['username'],
										'attempted_password'  => '',
										'attempted_ip'		  => $client_ip,
										'attempt_number'      => ATTEMPT_SUCCESS,
										'attempted_user_id'	  => $user_id,
										'attempted_timestamp' => CURRENT_DATE
									);
									$log_id 		= $this->log_general_attempts($log_general);
									$log_login  	= $this->log_login_attempts($log_attempt,$log_id);

									return $password_age;
					    		}
					    		else
					    		{
					    			return $password_age;
					    		}
				    		}
				    		else
				    		{
				    			return $last_activity;
				    		}
				    	}
				    	else
				    	{
				    		return $multiple_device;
						}

					}
		    	}
		    	else
		    	{
		    		//log user in table with attempt and password tried
					//Get initail count
		    		$count = $this->get_max_attempt_count_log($user_id, $username);
		    		//add password attempt
		    		$count = $count + 1;
		    		if($count > LOG_COUNT)
		    		{
		    			$this->inactive_user(BLOCKED,$user_id);
		    			return ['success' => false, 'code' => FAIL, 'msg' => 'You have reached the maximum level of password attempt. Please try login tomorrow.'];
					}
		    		else
		    		{
		    			//log the user when username is correct but password is wrong
						$general_pfail = array(
								'log_general_description' => 'user trying login',
								'log_general_event_type'  => LOGIN_FAIL_PASSWORD_WRONG,
								'created_timestamp'		  => CURRENT_DATE,
								'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
						);

						$attempt_pfail = array(
								'attempted_username'  => $data['username'],
								'attempted_password'  => $data['password'],
								'attempted_ip'		  => $client_ip,
								'attempt_number'      => ATTEMPT_FAIL,
								'attempted_user_id'	  => $user_id,
								'attempted_timestamp' => CURRENT_DATE
						);

						$log_id 		= $this->log_general_attempts($general_pfail);
						$this->log_login_attempts($attempt_pfail,$log_id);

		    			return ['success' => false, 'code' => FAIL, 'msg' => 'Password is wrong.'];
		    		}

		    	}
	    	} else
	    	{
	    		$general_pfail = array(
						'log_general_description' => 'user trying login',
						'log_general_event_type'  => LOGIN_FAIL_USER_INACTIVE,
						'created_timestamp'		  => CURRENT_DATE,
						'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
				);

				$attempt_pfail = array(
						'attempted_username'  => $data['username'],
						'attempted_password'  => '',
						'attempted_ip'		  => $client_ip,
						'attempt_number'      => ATTEMPT_SUCCESS,
						'attempted_user_id'	  => $user_id,
						'attempted_timestamp' => CURRENT_DATE
				);

				$log_id = $this->log_general_attempts($general_pfail);
				$this->log_login_attempts($attempt_pfail,$log_id);
	    		return $is_user_active;
			}
	    }
	    else
	    {
	    	//Get initail count
    		$count = $this->get_max_attempt_count_by_ip($client_ip);
    		//add password attempt
    		$count = $count + 1;
    		if($count > LOG_COUNT)
    		{
    			//$this->inactive_user(BLOCKED,$user_id);
    			return ['success' => false, 'code' => FAIL, 'msg' => 'Your ip has been blocked.'];
    		} else {
    			//log the user on successful login
				$general_ufail = array(
						'log_general_description' => 'user trying login',
						'log_general_event_type'  => LOGIN_FAIL_USER_WRONG,
						'created_timestamp'		  => CURRENT_DATE,
						'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
				);

				$attempt_ufail = array(
						'attempted_username'  => $data['username'],
						'attempted_password'  => $data['password'],
						'attempted_ip'		  => $client_ip,
						'attempt_number'      => ATTEMPT_FAIL,
						'attempted_user_id'	  => '0',
						'attempted_timestamp' => CURRENT_DATE
				);

				$log_id 		= $this->log_general_attempts($general_ufail);
				$this->log_login_attempts($attempt_ufail,$log_id);
			}
	       	return ['success' => false, 'code' => FAIL, 'msg' => 'Username or Password is wrong.'];
	    }
	}*/

	/*
	Function: is_user_active
	Desc: Check wheather user is active
	Date: 11/10/2018, 12/10/2018
	Developer: Gauri
	*/
	protected function is_user_active($user_id, $username)
	{
		$is_active = $this->conn->query("Select active from users where user_id =".$user_id)->fetch();
		if($is_active['active'])
		{
			$general_ufail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => USER_ACTIVE,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_ufail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_ufail);
			$this->log_login_attempts($attempt_ufail,$log_id);
			return ['success' => true, 'code' => SUCCESS, 'msg' => M1002];
		} else {
			$general_ufail = array(
					'log_general_description' => 'user trying login',
					'log_general_event_type'  => USER_INACTIVE,
					'created_timestamp'		  => CURRENT_DATE,
					'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
				);

				$attempt_ufail = array(
					'attempted_username'  => $username,
					'attempted_password'  => '',
					'attempted_ip'		  => $this->get_client_ip(),
					'attempt_number'      => ATTEMPT_SUCCESS,
					'attempted_user_id'	  => $user_id,
					'attempted_timestamp' => CURRENT_DATE
				);

				$log_id = $this->log_general_attempts($general_ufail);
				$this->log_login_attempts($attempt_ufail,$log_id);

				$error[] = ['msg' => USER_INACTIVE, "status" =>false];
			return ['success' => false, 'code' => FAIL, 'msg' => M1003];
		}
	}

	/*
	Function: is_multi_device
	Desc: Check weather user is active on multiple device with user ip address stored in db
	Date: 12/10/2018
	Developer: Neha
	*/
	protected function is_multi_device($user_id, $username)
	{
		$db_ip = $this->conn->query("Select user_ip,user_session_id from users where user_id =".$user_id)->fetch();
		$client_ip = $this->get_client_ip();
		//echo $client_ip;
		// echo $_SESSION["id"]; die;

		if(empty($db_ip['user_ip']) || empty($db_ip['user_session_id']))
		{
			//$userIP = $this->conn->query("Update users set user_ip = '".$client_ip."' where user_id =".$user_id)->fetch();
			$general_ufail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => USER_ACTIVE_SINGLE_DEVICE,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_ufail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_ufail);
			$this->log_login_attempts($attempt_ufail,$log_id);
			$result = ['msg' => USER_ACTIVE_SINGLE_DEVICE, "status" =>true, "data" => array()];
			return array('msg' => M1004, 'success' => true, 'code' => SUCCESS, 'result' => $result);

		}
		else if($db_ip['user_session_id'] != $_SESSION["id"])
		{
			$general_ufail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => USER_ACTIVE_MULTIPLE_DEVICE,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_ufail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_ufail);
			$this->log_login_attempts($attempt_ufail,$log_id);
			$result = ['msg' => USER_ACTIVE_MULTIPLE_DEVICE, "status" =>true, "data" => array()];
			return array('msg' => M1005, 'success' => false, 'code' => FAIL, 'result' =>$result);

		}
		else if($db_ip['user_ip'] != $client_ip)
		{
			$general_ufail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => USER_LOCATION_CHANGE,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_ufail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_ufail);
			$this->log_login_attempts($attempt_ufail,$log_id);
			$result = ['msg' => USER_LOCATION_CHANGE, "status" =>true, "data" => array()];
			return array('msg' => M1006, 'success' => false, 'code' => FAIL,'logout' => 1, 'result' => $result);

		}

		else
		{
			$general_ufail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => USER_ACTIVE_SINGLE_DEVICE,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_ufail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_ufail);
			$this->log_login_attempts($attempt_ufail,$log_id);
			return array('msg' => M1007, 'success' => true, 'code' => SUCCESS);
		}



		/// same session and ip different give message location change and logout user.
	}

	/*
	Function: check_last_activity_timeout
	Desc: Check last activity of user
	Date: 12/10/2018
	Developer: Neha
	*/
	protected function check_last_activity_timeout($user_id)
	{
		$db_timestamp = $this->conn->query("Select last_activity_timestamp from users where user_id =".$user_id)->fetch();
		$last_timestamp 	= strtotime($db_timestamp['last_activity_timestamp']);

		// if($last_timestamp >= strtotime(LAST_ACTIVITY))
		// {
			$current_time = date('Y-m-d H:i:s');
			$new_timestamp = $this->conn->query("Update users set last_activity_timestamp = '".$current_time."' where user_id =".$user_id)->fetch();
			return array('msg' => M1008, 'success' => true, 'code' => SUCCESS);
			//return ['success' => true, 'code' => SUCCESS, 'msg' => 'Last activity timeout is not old.'];
		// }
		// else
		// {
		// 	return ['success' => false, 'code' => FAIL, 'msg' => 'Last activity timeout is too old. Logout the user.'];
		// }
	}

	/*
	Function: check_password_age
	Desc: Check password age
	Date: 12/10/2018
	Developer: Gauri
	*/
	protected function check_password_age($user_id, $username)
	{
		$last_password = $this->conn->query("Select last_updated from password_log where user_id =".$user_id." order by last_updated desc LIMIT 1")->fetch();

		$now = time(); // or your date as well
		$your_date = strtotime($last_password['last_updated']);
		$datediff = $now - $your_date;

		$day_count = round($datediff / (60 * 60 * 24));
				// print_r($day_count);
		if($day_count > PASSWORD_AGE) {
			$general_ufail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => PASSWORD_AGED,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_ufail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_ufail);
			$this->log_login_attempts($attempt_ufail,$log_id);
			$error[] = ['msg' => PASSWORD_AGED, "status" =>false];
			return ['success' => false, 'code' => FAIL, 'msg' => M1009, 'redirect' => '/change_password'];
		} else {
			$general_ufail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => PASSWORD_NOT_AGED,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_ufail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_ufail);
			$this->log_login_attempts($attempt_ufail,$log_id);

			return ['success' => true, 'code' => SUCCESS, 'msg' => M1010];
		}
	}

	/*
	Function: get_max_attempt_count_log
	Desc: get attempt count by user id
	Date: 22/10/2018
	Developer: Neha
	*/
	private function get_max_attempt_count_log($user_id, $username)
	{
		$no_of_attempts = $this->conn->query("Select count(attempt_number) from log_login_attempts where (attempted_user_id =".$user_id." AND attempted_timestamp::date='".date('Y-m-d')."' AND attempt_number =1)")->fetch();
		if($no_of_attempts['count'] > LOG_COUNT)
		{
			//$this->inactive_user(BLOCKED,$user_id);
			$general_pfail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => LOGIN_FAIL_MAX_PASSWORD_ATTEMPT,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_pfail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $user_id,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_pfail);
			$this->log_login_attempts($attempt_pfail,$log_id);
		}
		return $no_of_attempts['count'];
	}

	/*
	Function: get_max_attempt_count_by_ip
	Desc: get attempt count by user ip
	Date: 22/10/2018
	Developer: Neha
	*/
	protected function get_max_attempt_count_by_ip($user_ip, $username)
	{
		$no_of_attempts = $this->conn->query("Select count(attempt_number) from log_login_attempts where (attempted_ip ='".$user_ip."' AND attempted_timestamp::date='".date('Y-m-d')."' AND attempt_number =1)")->fetch();
		$count = $no_of_attempts['count'] + 1;
		if($count > LOG_COUNT)
		{
			//$this->inactive_user(BLOCKED,$user_id);
			$general_pfail = array(
				'log_general_description' => 'user trying login',
				'log_general_event_type'  => IP_MAX_ATTEMPT_COUNT_REACHED,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_pfail = array(
				'attempted_username'  => $username,
				'attempted_password'  => '',
				'attempted_ip'		  => $user_ip,
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => 0,
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_pfail);
			$this->log_login_attempts($attempt_pfail,$log_id);

		} else {
			//log the user on successful login
			$general_ufail = array(
					'log_general_description' => 'user trying login',
					'log_general_event_type'  => IP_MAX_ATTEMPT_COUNT_NOT_REACHED,
					'created_timestamp'		  => CURRENT_DATE,
					'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_ufail = array(
					'attempted_username'  => $username,
					'attempted_password'  => '',
					'attempted_ip'		  => $user_ip,
					'attempt_number'      => ATTEMPT_SUCCESS,
					'attempted_user_id'	  => '0',
					'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_ufail);
			$this->log_login_attempts($attempt_ufail,$log_id);
		}

		return $no_of_attempts['count'];
	}

	/*
	Function: log_general_attempts
	Desc: Log in general table
	Date: 22/10/2018
	Developer: Neha
	*/
	protected function log_general_attempts($general)
	{
		$log = 	$this->conn->query("INSERT INTO log_general (log_general_description, log_general_event_type, created_timestamp, child_log) VALUES ('".$general['log_general_description']."', '".$general['log_general_event_type']."', '".$general['created_timestamp']."', '".$general['child_log']."') RETURNING log_general_id")->fetch();

		return $log['log_general_id'];
	}

	/*
	Function: log_login_attempts
	Desc: Log login attempts on fail or success
	Date: 22/10/2018
	Developer: Neha
	*/
	private function log_login_attempts($attempt,$logId)
	{
		//Enrypt password
		$log_attempt = $this->conn->query("INSERT INTO log_login_attempts (attempted_username, attempted_password, attempted_ip, attempt_number,attempted_user_id,attempted_timestamp,log_general_id) VALUES ('".$attempt['attempted_username']."', '".$attempt['attempted_password']."', '".$attempt['attempted_ip']."',".$attempt['attempt_number'].",".$attempt['attempted_user_id'].",'".$attempt['attempted_timestamp']."','".$logId."')")->fetch();
	}

	protected function inactive_user($status,$user_id)
	{
		$this->conn->query("Update users set active = ".$status." where user_id =".$user_id)->fetch();
	}

	/*
	Function: get_client_ip
	Desc: Fetch ip address of client
	Date: 12/10/2018
	Developer: Neha
	*/
	protected function get_client_ip()
	{
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}


	/*
	Function: is_user_exist
	Desc: check if user exist or not from username
	Date: 23/10/2018
	Developer: Neha
	*/
	protected function is_user_exist($data)
	{
		$success = $this->conn->query("SELECT * FROM users WHERE username='".$data."' AND active = ".ACTIVE." LIMIT 1")->fetch();
		if($success)
		{
			$_SESSION["user_id"] = $success['user_id'];
			$_SESSION["user_name"] = $success['username'];
			//log user in general table
			//log the user on successful login
			$log_general = array(
					'log_general_description' => FORGOT_PASSWORD_OTP_REQUEST_DESC,
					'log_general_event_type'  => FORGOT_PASSWORD_OTP_REQUEST,
					'created_timestamp'		  => CURRENT_DATE,
					'child_log'				  => json_encode(array('table'=>OTP_REQUEST))
			);
			$log_id 		= $this->log_general_attempts($log_general);

			//generate unique otp with expiry time and store in database
			$otp 	   = $this->generate_random_otp();
			$client_ip = $this->get_client_ip();
			$otp_array = array(
					'user_id'  			=> $success['user_id'],
					'log_general_id'  	=> $log_id,
					'attempted_ip'		=> $client_ip,
					'otp'      			=> $otp,
					'created_timestamp'	=> CURRENT_DATE,
					'expiry_timestamp' 	=> EXPIRY_DATE
			);

			$expiry_old = $this->expire_old_otp_details($otp_array);
			$add_otp  	= $this->insert_otp_details($otp_array);

			//*** TO-DO send otp to mobile device ***//
			$response = $this->send_OTP($success['user_id'], $otp);
			if(isset($response['success']) && !$response['success'])
			{
				return ['success' => false, 'code' => FAIL, 'msg' => $response['msg']];
			}
			return ['success' => true, 'code' => SUCCESS, 'msg' => M1011,'user_id'=>$_SESSION["user_id"]];
		}
		else
			return ['success' => false, 'code' => FAIL, 'msg' => M1012];
	}
	protected function send_OTP($user_id, $otp)
	{
		//Get phone number of user
		$user_data = $this->conn->query("Select user_phone_no from users where user_id =".$user_id)->fetch();
		$msg = "Your OTP to reset password is ".$otp.".";

		$to = $user_data['user_phone_no'];
		$url = "http://websmssinergiccoid-for.cloud.revoluz.io/playsms/index.php?app=ws&u=BPR-SINERGI&h=9c5734b65d6878973fbbad1bb96afd9f&op=pv&to=".$to."&msg=".urlencode($msg)."&nofooter=1";

		$ch = curl_init();
 		curl_setopt($ch, CURLOPT_URL, $url);
		// Set the result output to be a string.
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);
		// print_r($output);
		// die;
		if (curl_error($ch)) {
		    $error_msg = curl_error($ch);
		    return ['success' => false, 'msg' => $error_msg ];
		} else {
			$result = json_decode($output, true);
			if(!empty($result))
			{
				$status = $result['data'][0]['status'];
				$error = $result['data'][0]['error'];
				if($status == "OK" && $error == 0)
				{
					return ['success' => true , 'msg' => "Message for OTP sent successfully."];
				} else {
					return ['success' => false, 'msg' => $result['error_string']];
				}
			} else {
				return ['success' => false, 'msg' => "Something went wrong, Please try again." ];
			}
		}
	}

	private function generate_random_otp()
	{
		return mt_rand(100000, 999999);
	}

	/*
	Function: update_old_otp_details
	Desc: expire old otp for same user for same date
	Date: 23/10/2018
	Developer: Neha
	*/
	private function expire_old_otp_details($array)
	{
		$get_otp = $this->conn->query("SELECT * FROM one_time_passwords WHERE expiry_timestamp>'".EXPIRE_OTP."' AND user_id = ".$array['user_id']."")->fetchAll();;
		if(isset($get_otp) && !empty($get_otp))
		{
			foreach ($get_otp as $key => $value)
			{
				$this->conn->query("Update one_time_passwords set expiry_timestamp = '".$value['created_timestamp']."' where otp_id =".$value["otp_id"]."")->fetch();
			}
		}
	}
	/*
	Function: insert_otp_details
	Desc: generate otp and insert in database
	Date: 23/10/2018
	Developer: Neha
	*/
	private function insert_otp_details($array)
	{
		$new_otp = $this->conn->query("INSERT INTO one_time_passwords (user_id, log_general_id,otp, created_timestamp,expiry_timestamp) VALUES ('".$array['user_id']."', '".$array['log_general_id']."', '".$array['otp']."','".$array['created_timestamp']."','".$array['expiry_timestamp']."')")->fetch();

	}

	protected function is_otp_exist($data)
	{
		$success = $this->conn->query("SELECT * FROM one_time_passwords WHERE otp='".$data['otp']."' AND user_id = '".$data['user_id']."' AND expiry_timestamp > '".CURRENT_DATE."' LIMIT 1")->fetch();
		if($success)
		{
			// if($success['expiry_timestamp'] > CURRENT_DATE)
			// {
			//	return ['success' => true, 'code' => SUCCESS, 'msg' => 'Verification success.'];
			// }
			// else
			// {
			// 	return ['success' => false, 'code' => FAIL, 'msg' => 'Invalid OTP or OTP has been expired!!'];
			// } die;

			//*** TO-DO send otp to mobile device ***//

			//check count of otp attempts
			$attempt_count = $this->get_otp_attempt_count($data['user_id']);
			if($attempt_count >= LOG_COUNT)
			{
				$this->inactive_user(INACTIVE,$data['user_id']);
				return ['success' => false, 'code' => FAIL, 'msg' => 'You are blocked by the system.Please contact admin','user_blocked'=>1];
			}
			else
			{
				//log wrong otp inserted in general and attempt table
				$log_general = array(
						'log_general_description' => VALID_OTP_DESC,
						'log_general_event_type'  => VALID_OTP,
						'created_timestamp'		  => CURRENT_DATE,
						'child_log'				  => json_encode(array('table'=>OTP_ATTEMPT))
				);
				$log_id 		= $this->log_general_attempts($log_general);

				$client_ip = $this->get_client_ip();
				$otp_attempts = array(
						'otp_id'  			=> $success['otp_id'],
						'log_general_id'  	=> $log_id,
						'attempted_otp'		=> '',
						'attempt_number'	=> ATTEMPT_SUCCESS,
						'attempted_user_id'	=> $data['user_id'],
						'attempted_timestamp'	=> CURRENT_DATE,
						'attempted_ip'		=> $client_ip
				);
				$log_attempt  	= $this->log_otp_attempt($otp_attempts);
				return ['success' => true, 'code' => SUCCESS, 'msg' => M1013];
			}

		}
		else
		{
			//check count of otp attempts
			$attempt_count = $this->get_otp_attempt_count($data['user_id']);
			$attempt_count = $attempt_count + 1;
			if($attempt_count > LOG_COUNT)
			{
				$this->inactive_user(INACTIVE,$data['user_id']);
				return ['success' => false, 'code' => FAIL, 'msg' => M1014,'user_blocked'=>1];
			}
			else
			{
				//log wrong otp inserted in general and attempt table
				$log_general = array(
						'log_general_description' => INVALID_OTP_DESC,
						'log_general_event_type'  => INVALID_OTP,
						'created_timestamp'		  => CURRENT_DATE,
						'child_log'				  => json_encode(array('table'=>OTP_ATTEMPT))
				);
				$log_id 		= $this->log_general_attempts($log_general);

				$client_ip = $this->get_client_ip();
				$otp_attempts = array(
						'otp_id'  			=> 0,
						'log_general_id'  	=> $log_id,
						'attempted_otp'		=> $data['otp'],
						'attempt_number'	=> ATTEMPT_FAIL,
						'attempted_user_id'	=> $data['user_id'],
						'attempted_timestamp'	=> CURRENT_DATE,
						'attempted_ip'		=> $client_ip
				);
				$log_attempt  	= $this->log_otp_attempt($otp_attempts);
				return ['success' => false, 'code' => FAIL, 'msg' => M1015,'user_id'=>$data['user_id'],'user_name'=>$data['user_name']];
			}
		}
	}

	protected function log_otp_attempt($attempt)
	{
		$log_attempt = $this->conn->query("INSERT INTO log_otp_attempts (otp_id, log_general_id, attempted_otp, attempt_number,attempted_user_id,attempted_timestamp,attempted_ip) VALUES ('".$attempt['otp_id']."', '".$attempt['log_general_id']."', '".$attempt['attempted_otp']."','".$attempt['attempt_number']."','".$attempt['attempted_user_id']."','".$attempt['attempted_timestamp']."','".$attempt['attempted_ip']."')")->fetch();

	}

	/*
	Function: get_max_attempt_count_log
	Desc: get attempt count by user id
	Date: 22/10/2018
	Developer: Neha
	*/
	protected function get_otp_attempt_count($user_id)
	{
		$no_of_attempts = $this->conn->query("Select count(attempt_number) from log_otp_attempts where (attempted_user_id =".$user_id." AND attempt_number =1)")->fetch();

		return $no_of_attempts['count'];
	}


	/*
	Function: check_last_activity_timeout
	Desc: Check last activity of user
	Date: 12/10/2018
	Developer: Neha
	*/
	function check_last_activity_everytime()
	{

		$db_timestamp = $this->conn->query("Select last_activity_timestamp from users where user_id =".$_SESSION['user_id'])->fetch();
		$last_timestamp 	= strtotime($db_timestamp['last_activity_timestamp']);
		if($last_timestamp >= strtotime(LAST_ACTIVITY))
		{
			$new_timestamp = $this->conn->query("Update users set last_activity_timestamp = '".CURRENT_DATE."' where user_id =".$_SESSION['user_id'])->fetch();

			//return ['success' => true, 'code' => SUCCESS, 'msg' => 'Last activity timeout is not old.'];
		}
		else
		{
			$new_timestamp = $this->conn->query("Update users set user_ip = '',user_session_id = '', logged_in = 0, last_activity_timestamp = '".CURRENT_DATE."' where user_id =".$_SESSION["user_id"])->fetch();

			$general_pfail = array(
				'log_general_description' => 'user last activity timeout',
				'log_general_event_type'  => LAST_ACTIVITY_TIMEOUT,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_login_attempts'))
			);

			$attempt_pfail = array(
				'attempted_username'  => $_SESSION['user_name'],
				'attempted_password'  => '',
				'attempted_ip'		  => $this->get_client_ip(),
				'attempt_number'      => ATTEMPT_SUCCESS,
				'attempted_user_id'	  => $_SESSION['user_id'],
				'attempted_timestamp' => CURRENT_DATE
			);

			$log_id = $this->log_general_attempts($general_pfail);
			$this->log_login_attempts($attempt_pfail,$log_id);

			$logout_general_pfail = array(
				'log_general_description' => 'user logout',
				'log_general_event_type'  => USER_FORCEFULLY_LOGGED_OUT,
				'created_timestamp'		  => CURRENT_DATE,
				'child_log'				  => json_encode(array('table'=>'log_logout'))
			);


			$log_id = $this->log_general_attempts($logout_general_pfail);

			$log_logout = array(
				'log_general_id' => $log_id,
				'attempted_ip' => $this->get_client_ip(),
				'user_id' => $_SESSION["user_id"],
				'logout_timestamp' => CURRENT_DATE
			);

			$this->log_logout_log($log_logout);

		   	unset($_SESSION["id"]);
		   	unset($_SESSION["user_id"]);
		   	unset($_SESSION["user_name"]);
		   	return ['success' => false, 'code' => FAIL, 'msg' => M1016];
		}
	}
	protected function log_logout_log($log)
	{
		$log_attempt = $this->conn->query("INSERT INTO log_logout (log_general_id, attempted_ip, user_id, logout_timestamp) VALUES (".$log['log_general_id'].", '".$log['attempted_ip']."', ".$log['user_id'].",'".$log['logout_timestamp']."')")->fetch();
	}
	// protected function check_password_complexity()
	// {
	// 	//
	// 	// $new_password = 'test@123';
	// 	//Check conditions

	// }
	protected function get_menu_list($user_id)
	{
		//get from access template
		$templates = $this->conn->query("Select * from user_access where user_id =".$user_id)->fetch();

		if(!empty($templates)) {
			$template_id = $templates['template_id'];

			$route_data = $this->conn->query("Select * from page_access where template_id = ".$template_id)->fetchAll();
			// echo "<pre>";
			// print_r($route_data);
			// die;
			if(!empty($route_data)) {
				$route_ids = array();
				$route_ids[] = 0;
				foreach ($route_data as $key => $value) {
				# code...
					$route_ids[] = $value['route_id'];
				}

				$data = $this->conn->query("Select * from menus left join page_routes on menus.route_id = page_routes.route_id where menus.route_id = ANY('{".implode(',',$route_ids)."}')")->fetchAll();
				$menus = array();
				$sub_menus = array();
				foreach ($data as $key => $value) {
					# code...
					if($value['menu_parent_id'] == 0)
					{
						$temp_menus = array();
						$temp_menus['menu_item_id'] = $value['menu_item_id'];
						$temp_menus['menu_name'] = $value['menu_name'];
						$menus[] = $temp_menus;
					} else {
						$temp = array();
						$temp['route_id'] = $value['route_id'];
						$temp['route_url'] = $value['route_url'];
						// $temp['menu_item_id'] = $value['menu_item_id'];
						$temp['menu_parent_id'] = $value['menu_parent_id'];
						$temp['menu_name'] = $value['menu_name'];
						$sub_menus[] = $temp;
					}
				}
				$menu_data['menus'] = $menus;
				$menu_data['sub_menus'] = $sub_menus;
				// echo "<pre>";
				// print_r($menu_data);
				// die;
				return ['success' => true, 'code' => SUCCESS, 'msg' => 'data found', 'data' => $menu_data];
			} else {
				return ['success' => false, 'code' => FAIL, 'msg' => 'No route data found'];
			}
		} else {
			return ['success' => false, 'code' => FAIL, 'msg' => 'No template found'];
		}

	}


}