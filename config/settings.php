<?php

$settings = [];

// Slim settings
$settings['displayErrorDetails'] = true;
$settings['determineRouteBeforeAppMiddleware'] = true;

// Path settings
$settings['root'] = dirname(__DIR__);
$settings['temp'] = $settings['root'] . '/tmp';
$settings['public'] = $settings['root'] . '/public';

// View settings
$settings['twig'] = [
	'debug' => true,
    'path' => $settings['root'] . '/templates',
    'cache_enabled' => false,
    'cache_path' =>  $settings['temp'] . '/twig-cache'
];

//PDO

$settings['pdo']['engine'] 		= 'pgsql';
// $settings['pdo']['host'] 		= 'localhost';
// $settings['pdo']['database'] 	= 'core_functionality';
// $settings['pdo']['username'] 	= 'core';
// $settings['pdo']['password'] 	= 'bitware@core!@#';
$settings['pdo']['host'] 		= '159.65.72.21';
$settings['pdo']['database'] 	= 'core_functionality';
$settings['pdo']['username'] 	= 'postgres';
$settings['pdo']['password'] 	= 'Szxyt!@#456';
// $settings['pdo']['collation'] 	= 'utf8_unicode_ci';
$settings['pdo']['options'] 	= array(PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES   => true);

return $settings;
