<?php

use Slim\Container;


$container = $app->getContainer();

// Activating routes in a subfolder
$container['environment'] = function () {
    $scriptName = $_SERVER['SCRIPT_NAME'];
    $_SERVER['SCRIPT_NAME'] = dirname(dirname($scriptName)) . '/' . basename($scriptName);
    return new Slim\Http\Environment($_SERVER);
};

// Register Twig View helper -- added by us.
$container['view'] = function (Container $container) {
    $settings = $container->get('settings');
    $viewPath = $settings['twig']['path'];

    $twig = new \Slim\Views\Twig($viewPath, [
        'cache' => $settings['twig']['cache_enabled'] ? $settings['twig']['cache_path'] : false
    ]);

    /** @var Twig_Loader_Filesystem $loader */
    $loader = $twig->getLoader();
    $loader->addPath($settings['public'], 'public');

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment($container->get('environment'));
    $twig->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    $twig->addExtension(new Twig_Extension_Debug());

    $twig->getEnvironment()->addGlobal('session', $_SESSION);

    return $twig;
};
//-- end of view container -- 

$container['pdo'] = function (Container $container) {
    return $container->get('pdo')->getPdo();
};

$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container['pdo'] = function (Container $container) {
    // better load the settings with $container->get('settings')
    $settings = $container->get('settings');
    $host = $settings['pdo']['host'];
    $dbname = $settings['pdo']['database'];
    $username = $settings['pdo']['username'];
    $password = $settings['pdo']['password'];
    $charset = 'utf8';
    $collate = 'utf8_unicode_ci';
    // $dsn = "pgsql:host=$host;dbname=$dbname";

    // $dbh = new PDO("pgsql:host=$host;dbname=$dbname",$username,$password);
    $dsn = "pgsql:host=$host;port=5432;dbname=$dbname;user=$username;password=$password";
    $dbh = new PDO($dsn);

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
};

$container['UserController'] = function ($container) {
 return new \App\Controllers\UserController($container);
};

// Register session globally to app
// $container['session'] = function ($c) {
//   return new \SlimSession\Helper;
// };
