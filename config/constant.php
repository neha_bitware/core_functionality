<?php

	define('BASE_URL', '/core_functionality/');
	define("LOG_COUNT", 3);
	define("PASSWORD_AGE", 30);
	define("LAST_ACTIVITY", "-10 minutes");
	define("SUCCESS", 200);
	define("FAIL", 401);
	//define("ACTIVITY_FAIL", 402);
	define("ACTIVE", 1);
	define("INACTIVE", 0);
	define("BLOCKED", 2);

	//define("LOG_EVENT_TYPE_SECONDARY", "secondary");
	define("LOG_EVENT_TYPE_LOGIN", "login");
	define("CURRENT_DATE", date('Y-m-d H:i:s'));
	define("EXPIRY_DATE", date("Y-m-d H:i:s", strtotime('+5 minutes', strtotime(CURRENT_DATE))));
	define("EXPIRE_OTP", date("Y-m-d H:i:s", strtotime('-5 minutes', strtotime(CURRENT_DATE))));
	//LOG CONSTANTS
	// LOGIN
	define("LOGIN_SUCCESS", "login success");
	define("LOGIN_FAIL", "login failed");
	define("LOGIN_FAIL_PASSWORD_WRONG", "login fail password wrong");
	define("LOGIN_FAIL_MAX_PASSWORD_ATTEMPT", "login fail max password attempt reached");
	define("LOGIN_FAIL_USER_WRONG", "login fail username or password wrong");
	define("LOGIN_FAIL_USER_INACTIVE", "login fail user is inactive");
	define("ATTEMPT_SUCCESS", 0);
	define("ATTEMPT_FAIL", 1);

	//FORGOT PASSWORD OTP
	define("FORGOT_PASSWORD_OTP_REQUEST", "request_forgot_password_otp");
	define("FORGOT_PASSWORD_OTP_REQUEST_DESC", "user request for forgot_password_otp");
	define("INVALID_OTP", "invalid_otp_inserted");
	define("INVALID_OTP_DESC", "user inserted invalid_otp");
	define("VALID_OTP", "valid_otp_inserted");
	define("VALID_OTP_DESC", "user inserted valid_otp");



	//TABLE NAMES
	define("LOGIN_ATTEMPT","log_login_attempts");
	define("GENERAL_ATTEMPT","log_general");
	define("OTP_REQUEST","one_time_passwords");
	define("OTP_ATTEMPT","log_otp_attempts");

	//Login Process
	define("VALID_USERNAME", "username is valid");
	define("INVALID_USERNAME", "username is invalid");
	define("ALREADY_LOGIN", "user is already logged in");
	define("USER_LOGIN_ATTEMPT", "user trying to login");
	define("USER_ACTIVE", "user is active");
	define("USER_INACTIVE", "user is inactive");
	define("USER_ACTIVE_SINGLE_DEVICE", "user active on single device");
	define("USER_LOCATION_CHANGE", "user location has been changed");
	define("USER_ACTIVE_MULTIPLE_DEVICE", "user active on multiple device");
	define("PASSWORD_NOT_AGED", "user password is not aged");
	define("PASSWORD_AGED", "user password is too old");
	define("IP_MAX_ATTEMPT_COUNT_REACHED", "ip max attempt count reached");
	define("IP_MAX_ATTEMPT_COUNT_NOT_REACHED", "ip max attempt count not yet reached");
	define("LAST_ACTIVITY_TIMEOUT", "last activity timeout");
	define("USER_LOGOUT", "user logged out");
	define("USER_FORCEFULLY_LOGGED_OUT", "forcefully logged out");

	define("PASSWORD_CHANGED", 'user_changed_password');
	define("PASSWORD_CHANGED_DESC", 'user has changed the password');
	define("PASSWORD_COMPLEXITY", 'password_complexity');
	define("COMPLEXITY_ID", 3);