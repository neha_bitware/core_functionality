<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->map(['GET', 'POST'],'/', \App\Controllers\UserController::class . ':user_login');
$app->map(['GET', 'POST'],'/login', \App\Controllers\UserController::class . ':user_login');

$app->map(['GET', 'POST'],'/dashboard', \App\Controllers\UserController::class . ':user_login_process');
$app->get('/logout', \App\Controllers\UserController::class . ':user_logout');
$app->map(['GET', 'POST'],'/forgot_password', \App\Controllers\UserController::class . ':user_forgot_password');
$app->map(['GET', 'POST'],'/verify_otp', \App\Controllers\UserController::class . ':verify_otp_process');
$app->map(['GET', 'POST'],'/new_password', \App\Controllers\UserController::class . ':create_new_password');

$app->map(['GET', 'POST'],'/change_password', \App\Controllers\UserController::class . ':user_change_password');

$app->map(['GET', 'POST'],'/otp_resend', \App\Controllers\UserController::class . ':resend_otp');

$app->map(['GET', 'POST'],'/check_password', \App\Controllers\PasswordController::class . ':check_password_complexity');

$app->map(['GET', 'POST'],'/profile', \App\Controllers\UserAccountController::class . ':get_user_profile');

$app->map(['GET', 'POST'],'/account_details', \App\Controllers\UserAccountController::class . ':get_user_account_detail');

$app->map(['GET', 'POST'],'/transactions', \App\Controllers\BillingAndPaymentsController::class . ':get_transaction_history');

$app->map(['GET', 'POST'],'/transfer', \App\Controllers\BillingAndPaymentsController::class . ':transfer_payments');

//Admin Routes
$app->map(['GET', 'POST'],'/admin/login', \App\Controllers\Admin\LoginController::class . ':login');
$app->map(['GET', 'POST'],'/admin', \App\Controllers\Admin\LoginController::class . ':login');

$app->map(['GET', 'POST'],'/admin/dashboard', \App\Controllers\Admin\LoginController::class . ':login_process');
$app->get('/admin/logout', \App\Controllers\Admin\LoginController::class . ':admin_logout');

$app->map(['GET', 'POST'],'/admin/add_user', \App\Controllers\Admin\UserController::class . ':add_user');
$app->map(['GET', 'POST'],'/admin/edit_user', \App\Controllers\Admin\UserController::class . ':edit_user');
$app->map(['GET', 'POST'],'/admin/update_user', \App\Controllers\Admin\UserController::class . ':update_user');
$app->map(['GET', 'POST'],'/admin/delete_user', \App\Controllers\Admin\UserController::class . ':delete_user');
$app->map(['GET', 'POST'],'/admin/users', \App\Controllers\Admin\UserController::class . ':index');
$app->map(['GET', 'POST'],'/admin/assign_template', \App\Controllers\Admin\UserController::class . ':assign_template_to_user');

$app->map(['GET', 'POST'],'/admin/create_template', \App\Controllers\Admin\TemplateController::class . ':create');
$app->map(['GET', 'POST'],'/admin/update_template', \App\Controllers\Admin\TemplateController::class . ':update');
$app->map(['GET', 'POST'],'/admin/templates', \App\Controllers\Admin\TemplateController::class . ':index');
$app->map(['GET', 'POST'],'/admin/assign_routes', \App\Controllers\Admin\TemplateController::class . ':assign');
$app->map(['GET', 'POST'],'/admin/edit_template', \App\Controllers\Admin\TemplateController::class . ':edit');
$app->map(['GET', 'POST'],'/admin/get_users', \App\Controllers\Admin\TemplateController::class . ':get_role_and_dept_based_users');
$app->map(['GET', 'POST'],'/admin/check_user_template', \App\Controllers\Admin\TemplateController::class . ':ajax_check_user_template');
// $app->map(['GET', 'POST'],'/admin/user_list', \App\Controllers\Admin\UserController::class . ':list_all_users');
$app->map(['GET', 'POST'],'/admin/menus', \App\Controllers\Admin\MenuController::class . ':index');
$app->map(['GET', 'POST'],'/admin/edit_menu_structure', \App\Controllers\Admin\MenuController::class . ':edit_menu_structure');
$app->map(['GET', 'POST'],'/admin/update_menu_structure', \App\Controllers\Admin\MenuController::class . ':update_menu_structure');