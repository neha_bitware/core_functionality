
<?php

define('M1001', 'User already login and trying to log in from other device.');
define('M1002', 'User is active');
define('M1003', 'User is inactive. Please contact the admin.');
define('M1004', 'Not active on multiple device');
define('M1005', 'You are active on multiple devices. Please logout from another device first.');
define('M1006', 'You location has been changed.');
define('M1007', 'Not active on multiple device.');
define('M1008', 'Last activity timeout is not old.');
define('M1009', 'Your account password is too old.');
define('M1010', 'Password valid. Login successful.');
define('M1011', 'Enter OTP.');
define('M1012', 'User doesnot exist in our system Or user is inactive.');
define('M1013', 'Verification success.');
define('M1014', 'You are blocked by the system.Please contact admin.');
define('M1015', 'Invalid OTP or OTP has been expired!!');
define('M1016', 'Your session has been expired please login again.');
define('M1017', 'You have reached the maximum level of password attempt. Please try login tomorrow.');
define('M1018', 'Password is wrong.');
define('M1019', 'Your ip has been blocked.');
define('M1020', 'Username or Password is wrong.');
define('M1021', 'You are blocked by the system.Please contact admin.');
define('M1022', 'Enter Number only.');
define('M1023', 'Password successfully changed. Please login with new password.');
define('M1024', 'New password and Confirm password is not same.');
define('M1025', 'Password successfully changed. Please login with new password.');
define('M1026', 'New password and Confirm password is not same.');
define('M1027', 'Old Password does not match with record.');
define('M1028', 'OTP resend successful.');
?>